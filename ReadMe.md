### Features

- The source code of the developed tool can be found in the SourceCode folder
- The logs used in the evaluation can be found in the EvaluationLogs folder
- A (windows-based) executable version of the tool can be found in the compressed ExecutableSoftware folder. To run it, simply extract files and navigate to the executable OCELConverter that can be found within the OCELConverter folder (look for an icon of a graph with 4 vertices)